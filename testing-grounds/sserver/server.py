import socket, threading, mimetypes

import ast

import matplotlib
import matplotlib.pyplot as plt

import datetime, time

import sys, os.path

# def run_http():
# 	http_handler = http.server.SimpleHTTPRequestHandler
# 	http_page = socketserver.TCPServer(("",80), http_handler)
# 	http_page.timeout = 1
# 	try:
# 		while (is_alive):
# 			http_page.handle_request()
# 	except Exception as exc_id:
# 		print(exc_id)
# 		pass
# 	http_page.server_close()


# http_handler_thread = threading.Thread(target=run_http)
# #http_handler_thread.daemon=True
# http_handler_thread.start()

# self.plots = [[],[],[]]
# port = 3714

# update_counts = 4320

# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# s.bind(('', port))
# s.listen(1)
# while(1):
# 	try:
# 		conn, addr = s.accept()
# 		try:
# 			#print(addr)
# 			data = conn.recv(4096)
# 			#print(data)
# 			data_store = json.loads(data)
# 			#print(data_store)

# 			storageFile = open("data.csv", 'w')
# 			storageFile.write("Temperature,{}°C\n".format(int(data_store[1])))
# 			storageFile.write("Humidity,{}%\n".format(int(data_store[2])))
# 			storageFile.close()

			
# 			self.plots[0].append(matplotlib.dates.date2num(datetime.datetime.strptime(time.ctime(data_store[0]), "%a %b %d %H:%M:%S %Y")))
# 			if (len(self.plots[0]) > update_counts):
# 				self.plots[0] = self.plots[0][-update_counts:]
# 			self.plots[1].append(data_store[1])
# 			if (len(self.plots[1]) > update_counts):
# 				self.plots[1] = self.plots[1][-update_counts:]
# 			self.plots[2].append(data_store[2])
# 			if (len(self.plots[2]) > update_counts):
# 				self.plots[2] = self.plots[2][-update_counts:]
# 			plt.plot_date(self.plots[0], self.plots[1], 'b-')
# 			#plt.plot_date(self.plots[0], self.plots[2], 'r-')
# 			plt.gcf().autofmt_xdate()
# 			plt.annotate(str(self.plots[1][-1]) + "°C", xy=(self.plots[0][-1], self.plots[1][-1]))
# 			#plt.annotate(str(self.plots[2][-1]) + "%", xy=(self.plots[0][-1], self.plots[2][-1]))
# 			plt.savefig('temp.png')
# 			plt.clf()
# 		except:
# 			print("data failure")
# 		conn.close()
# 	except KeyboardInterrupt:
# 		is_alive = False
# 		http_handler_thread.join()
# 		s.close()
# 		break


class ServerManager():
	def __init__(self):
		threading.Thread.__init__(self)

		self.port = 3714
		self.BUFFER_SIZE = 8192

		self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

		self.plots = [[],[],[]]
		self.updateCouts = 1310

		self.dataRunningAverageVal = [None,None]
		self.dataRunningAverage = [[],[]]
		self.dataAverageCount = 6

		try:
			self.server.bind(('', self.port))
		except socket.error:
			print('error: {}'.format(socket.error))
			sys.exit()

		self.server.listen(1)

	def doGET(self, msg, conn, addr):
		httpFile = ""

		if msg == "/" or msg == "/index.html":
			httpFile = "/index.html"
		else:
			if os.path.isfile(msg[1:]):
				if msg in ["/data.csv", "/assets/css/styles.css", "/0.png", "/1.png"]:
					httpFile = msg
				else:
					self.do403(msg, conn, addr)
					return
			else:
				httpFile = msg

		try:
			tempfile = open(httpFile[1:], 'rb')
			httpResp = tempfile.read()
			tempfile.close()
		except:
			self.do404(msg, conn, addr)
			return
		
		httpHeader="HTTP/1.1 200 OK\r\n"
		httpHeader+="Connection: keep-alive\r\n"
		httpHeader+="Keep-Alive: timeout=2\r\n"
		httpHeader+="Content-Length: {}\r\n".format(len(httpResp))
		httpHeader+="Content-Type: {}\r\n\r\n".format(mimetypes.guess_type(httpFile)[0])
		
		conn.send(httpHeader.encode('utf-8'))

		for i in range(0, len(httpResp), self.BUFFER_SIZE):
			end = min(i + self.BUFFER_SIZE, len(httpResp))
			conn.send(httpResp[i: end])

	def doPOST(self, msg, conn, addr):
		return

	def doDATA(self, msg, conn, addr):
		sensorData = ast.literal_eval(msg)
		sensorData = [sensorData[0]] + [round(i, 2) for i in sensorData[1:]]

		storageFile = open("data.csv", 'w')
		storageFile.write("Temperature,{}°C\n".format(sensorData[1]))
		storageFile.write("Humidity,{}%\n".format(sensorData[2]))
		storageFile.close()

		self.plots[0].append(matplotlib.dates.date2num(datetime.datetime.strptime(time.ctime(sensorData[0]), "%a %b %d %H:%M:%S %Y")))
		if (len(self.plots[0]) > self.updateCouts):
			self.plots[0].pop(0)

		dataGraphFormat = ['b', 'r']

		for i in range(len(self.dataRunningAverageVal)):
			if self.dataRunningAverageVal[i] == None:
				self.dataRunningAverageVal[i] = sensorData[i+1]
			else:
				self.dataRunningAverageVal[i] = round((self.dataRunningAverageVal[i] * (self.dataAverageCount-1) + sensorData[i+1]) / self.dataAverageCount,2)
			self.dataRunningAverage[i].append(self.dataRunningAverageVal[i])
			if (len(self.dataRunningAverage) > self.updateCouts):
				self.dataRunningAverage[i].pop(0)

		#print(self.dataRunningAverage)

		for i, data in enumerate(sensorData[1:]):
			self.plots[i+1].append(sensorData[i+1])
			if (len(self.plots[i+1]) > self.updateCouts):
				self.plots[i+1].pop(0)
			plt.plot_date(self.plots[0], self.plots[i+1], dataGraphFormat[i]+"-")
			plt.plot_date(self.plots[0], self.dataRunningAverage[i], "k--")
			plt.gcf().autofmt_xdate()
			plt.savefig('{}.png'.format(i))
			plt.clf()

		return

	def do404(self, msg, conn, addr):
		ERROR_404 = '<!DOCTYPE html><html><body><h1>Error 404: Page Not Found</h1></body</html>'
		conn.send(bytes("HTTP/1.1 404 Not Found\r\n\r\n".encode('utf-8')))
		conn.send(bytes(ERROR_404.encode('utf-8')))
		conn.close()
		return

	def do403(self, msg, conn, addr):
		print("connection {} requesting {}. refused".format(addr, msg))
		ERROR_403 = '<!DOCTYPE html><html><body><h1>Error 403: Kindly fuck off.</h1></body</html>'
		conn.send(bytes("HTTP/1.1 403 Forbidden\r\n\r\n".encode('utf-8')))
		conn.send(bytes(ERROR_403.encode('utf-8')))
		conn.close()
		return

	def run_thread(self, conn, addr):
		# connection timeout after 60-second inactivity
		conn.settimeout(5.0)

		ftt = True

		while True:
			try:
				httpmsg = conn.recv(self.BUFFER_SIZE)
				if not httpmsg: break
				elif httpmsg == bytes("\r\n",'utf-8'): break

				httpdec = httpmsg.decode().split('\r\n')
				httpreq = httpdec[0].split()

				if not (httpreq[0][0] == "[" and httpreq[0][-1] == "]"):
					print(addr[0] + ':' + str(addr[1]), httpdec[0])

					if ftt:
						logfile = open("server-log.txt", 'a')
						logfile.write(str(addr) + "\n" + httpmsg.decode()+"\n")
						logfile.close()
						ftt = False

				if httpreq[0] == "GET":
					self.doGET(httpreq[1], conn, addr)
				elif httpreq[0] == "POST":
					self.doPOST([], conn, addr)
				elif httpreq[0][0] == "[" and httpreq[0][-1] == "]":
					self.doDATA(httpreq[0], conn, addr)
					break
				else:
					self.do404(httpreq[1], conn, addr)

			except socket.timeout:
				#Socket timeout
				#print("error: conn socket timeout")
				break

			except socket.error as e:
				#Other socket exceptions
				print("error: socket error {}".format(e))
				break

		conn.close() # Close socket
		#print ("connection: {} closed".format(addr))
		return

	def run(self):
		while True:
			#print(threading.active_count())
			try:
				conn, addr = self.server.accept()
				threading.Thread(target=self.run_thread, args=(conn, addr)).start()
			except:
				break
		self.exit()

	def exit(self):
		self.server.close()
		sys.exit()


if __name__=='__main__':
	server = ServerManager()
	try:
		server.run()
	except:
		server.exit()