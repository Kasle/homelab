#include "configuration.h"

LocalConfig::LocalConfig()
{
  /*this->config.lc_server_delay = 100;
  this->config.lc_sensor_delay = 10000;
  this->config.lc_serial_delay = 100;
  strcpy(this->config.lc_node_name, "Test Node 1");*/
}

int LocalConfig::begin(){
  Serial.printf("info: F_EEPROM usage is %d bytes\n", sizeof(this->config) + 1);
  EEPROM.begin(sizeof(this->config) + 1); //data+flag
  this->read();
  return 0;
}

int LocalConfig::write(){
  this->update_bytes(1, this->config);
  EEPROM.commit();
  return 0;
}

int LocalConfig::read(){
  this->read_bytes(1, this->config);
  return 0;
}
