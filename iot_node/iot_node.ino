#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <time.h>
#include <Wire.h>
#include "wifi_access.h"
#include "bme_sensor.h"
#include "configuration.h"

//State stuff
#define MANAGER 	0
#define SLEEP 		1
#define UPTIME 		2
#define UPSENSOR 	3
#define UPSERIAL 	4
#define SERVER		5

unsigned short state_now;
unsigned short state_next;

//time configuration
unsigned long tc_time[2];
unsigned long tc_sensor[2] = {millis(), 10000};
unsigned long tc_serial[2];
unsigned long tc_server[2] = {millis(), 1000};

LocalConfig lc;

//WiFi Stuff
const char* ssid     = WIFI_SSID;
const char* password = WIFI_PWD;

ESP8266WebServer local_server(80);

//Network time
//time_t time_t_now;

//Remote stuff
long update_remote_time = 0;

//Sensor stuff
BME680 s_bme;
long next_sensor_update = 0;
int light_level = 0;
bool is_data_valid = false;

//operation stuff
long w_time_check = 0;
long w_time_set = 0;
int water_threshold = 0;

//EEPROM Configuration Data
int eeprom_address = 0;
int eeprom_size = 0;

void setup() {
	Serial.begin(115200);
 
  pinMode(13, OUTPUT);
  pinMode(12, INPUT);
  digitalWrite(13, LOW);
  
	Serial.printf("info: connecting to %s using password %s ", ssid, password);

	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	//WiFi.begin();

	local_server.begin();

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	Serial.println();
	Serial.println("info: wifi connected");
	Serial.printf("info: ip:%s\n", WiFi.localIP().toString().c_str());

	lc.begin();
	//lc.write_default();
	//lc.update_bytes();

	//if network time is needed
	/*
	configTime(-5*3600, 0, "time-a-g.nist.gov", "time.nist.gov");

	while (!time_t_now){
		time(&time_t_now);
		tc_time = millis();
		Serial.print(".");
		delay(1000);
	}


	Serial.println();

	Serial.println("info: time server connected");
	*/

	if (s_bme.init()){
		Serial.println("info: BME680 setup failed");
	} else {
		if (s_bme.configure()){
			Serial.println("info: BME680 configuration failed");
		} else {
			Serial.println("info: BME680 init and config complete");
		}
	}



  local_server.on("/config", handlePost);
  local_server.onNotFound(handleNotFound);
  local_server.begin();

	Serial.println("info: starting. waiting 5000");

	state_now = 0;
	state_next = 0;

	delay(5000);

  Serial.println("info: delay complete");
}

void handleNotFound(){
  local_server.send(404, "text/plain", "Not Found.\n");
}

void handlePost(){
  for (int i = 0; i < local_server.args()-1; i++){
    const char* server_arg_name = local_server.argName(i).c_str();
    const char* server_arg_value = local_server.arg(i).c_str();

    if (!strcmp(server_arg_name,"tc_server")) {
      tc_server[1] = atoi(server_arg_value);
      Serial.printf("info: %s configured: %d\n", server_arg_name, atoi(server_arg_value));
    } else if (!strcmp(server_arg_name,"tc_sensor")) {
      tc_sensor[1] = atoi(server_arg_value);
      Serial.printf("info: %s configured: %d\n", server_arg_name, atoi(server_arg_value));
    }

  }
  local_server.send(200, "text/plain", "OK.\n");
}

//If netowrk time is needed
/*
int update_time(){
	if ((unsigned long)(millis()-tc_time) <= 4000){
		Serial.println("error: time checked too fast");
		return 1;
	}
	time(&time_t_now);
	tc_time = millis();
	while (time_t_now < 1546322400){
		char print_buffer[100];
		snprintf(print_buffer, sizeof(print_buffer), "error: time %ld unreliable. retrying in 5000", time_t_now);
		Serial.println(print_buffer);
		delay(5000);
		time(&time_t_now);
		tc_time = millis();
	}

	char print_buffer[100];
	snprintf(print_buffer, sizeof(print_buffer), "info: re-synced time. now %ld", time_t_now);
    Serial.println(print_buffer);
	return 0;
}
*/

int update_sensor(){
  is_data_valid = true;
	return s_bme.read();
}

int update_local_sensors(){
  light_level = analogRead(A0);
  //Serial.printf("info: light level: %d\n", light_level);
  return 0;
}

int update_remote(){
	if (WiFi.status()== WL_CONNECTED){
		HTTPClient http;

    http.begin(REMOTE_DB);
		http.addHeader("Content-Type", "text/plain");

		char post_buffer[256];
		snprintf(post_buffer, sizeof(post_buffer), "temperature,host=hp1 value=%f\nhumidity,host=hp1 value=%f\npressure,host=hp1 value=%f\niaq,host=hp1 value=%f\nsoil,host=hp1 value=%d\nlight_level,host=hp1 value=%d\n", s_bme.temperature, s_bme.humidity, s_bme.pressure/100, s_bme.iaq, water_threshold, light_level);
		int httpCode = http.POST(post_buffer);

		Serial.println(post_buffer);
		Serial.printf("info: remote data sent. recieved %d\n", httpCode);
		//Serial.println(http.getString());

		http.end();
	} else {
		return 1;
	}
	return 0;
}

void loop() {
	state_now = state_next;
	switch(state_now){
		case MANAGER:
   
      if (w_time_set > millis()){
        //Serial.printf("info: watering from %ld until %ld...\n", millis(), w_time_set);
        digitalWrite(13, HIGH);
      } else {
        digitalWrite(13, LOW);
      }
      
			//update_time();

      if (w_time_check < millis()){
        w_time_check = millis() + 15000;
        
        if (digitalRead(12)){
          water_threshold = 0;
        } else {
          water_threshold = 1;
        }

        if (water_threshold == 0){
          Serial.println("info: soil moisture low. watering");
          w_time_set = millis() + 1000;
        }
      }
      
      if (next_sensor_update < millis()){
        next_sensor_update = millis() + 5000;
        
        update_sensor();
        update_local_sensors();
        
        //Serial.println(s_bme.iaq);
      }
      
			if (update_remote_time < millis()){
        if (is_data_valid && s_bme.humidity != 0.0) {
			    update_remote();
        }
        update_remote_time = millis() + 10000;
			}

      state_next = UPSERIAL;
      
			/*if ((millis() - tc_server[0]) >= tc_server[1]){
				tc_server[0] = millis();
				state_next = SERVER;
			}
			else {
				state_next = UPSERIAL;
			}*/
			break;
     
    case UPSERIAL:
      if (Serial.available()){
        String readl = Serial.readString();
        readl.trim();
        //Serial.println("info: got " + readl);
        if (readl == "w"){
          Serial.println("info: set watering 5000");
          w_time_set = millis() + 5000;
        } else if (readl == "s") {
          Serial.printf("info: sensor data: %f, %f, %f, %f\n", s_bme.temperature, s_bme.humidity, s_bme.pressure/100, s_bme.iaq);
        } else if (readl == "l"){
          Serial.printf("info: light level: %d\n", light_level);
        } else if (readl == "m"){
          Serial.printf("info: soil moisture: %d\n", water_threshold);
        }
      }
      state_next = SLEEP;
      break;

		case SERVER:
			//Serial.printf("server.\n");
			local_server.handleClient();
			state_next = MANAGER;
			break;

		case SLEEP:
		default:
			//Serial.println("info: sleeping 10000);
			//delay(10000);
			//delay(500);
			state_next = MANAGER;
			break;
	}
}
