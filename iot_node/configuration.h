#ifndef configuration_h
#define configuration_h

#include <EEPROM.h>
#include <Arduino.h>

class LocalConfig
{
  public:
    LocalConfig();
    int begin();
    int write();
    int read();
    int available();
    int write_default();

    struct Config{
      unsigned long lc_server_delay, lc_sensor_delay,lc_serial_delay;
      char lc_node_name[20];
    } config;

    template<typename T>
    void update_bytes(int addr, T bytes){
      unsigned char byte_buffer[sizeof(bytes)];
      memcpy(&byte_buffer, &bytes, sizeof(bytes));
      for (int i = 0; i < sizeof(bytes);i++){
        if (EEPROM.read(addr+i) != byte_buffer[i]){
          Serial.printf("info: Writing %d to F_EEPROM address %d\n", byte_buffer[i], addr+i);
          EEPROM.write(addr+i, byte_buffer[i]);
        }
      }
    }

    template<typename T>
    void read_bytes(int addr, T &bytes){
      unsigned char byte_buffer[sizeof(bytes)];
      for (int i = 0; i < sizeof(bytes);i++){
        byte_buffer[i] = EEPROM.read(addr+i);
      }
      memcpy(&bytes, &byte_buffer, sizeof(bytes));
    }
};

#endif
