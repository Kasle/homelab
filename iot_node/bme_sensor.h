#ifndef bme_sensor_h
#define bme_sensor_h

#include <Adafruit_BME680.h> 
#include <bme680.h>
#include <bme680_defs.h>
#include <Arduino.h>

class BME680
{
  public:
    BME680();
    int init();
    int configure();
    int read();
    float get_iaq();
    float temperature;
    float humidity;
    float pressure;
    float gas_resistance;
    float gas_reference;
    float iaq;
  private:
    bool initialized;
    Adafruit_BME680 bme;
};

#endif
