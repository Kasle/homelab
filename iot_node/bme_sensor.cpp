#include "bme_sensor.h"

BME680::BME680()
{
  this->initialized = false;
  this->temperature = 0;
  this->humidity = 0;
  this->pressure = 0;
  this->gas_resistance = 0;
  this->iaq = 0;
  this->gas_reference=50000;
}

int BME680::init(){
  if (!this->bme.begin()){
		return 1;
	}
  this->initialized = true;
  return 0;
}

int BME680::configure(){
  if (!this->initialized){
      return 1;
  }
  this->bme.setTemperatureOversampling(BME680_OS_8X);
	this->bme.setHumidityOversampling(BME680_OS_2X);
	this->bme.setPressureOversampling(BME680_OS_4X);
	this->bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
	this->bme.setGasHeater(320, 100); // 320*C for 150 ms
  return 0;
}

float BME680::get_iaq(){
  float hum_score, gas_score;
  if (this->humidity >= 38 && this->humidity <= 42)
    hum_score = 0.25*100; // Humidity +/-5% around optimum 
  else
  { //sub-optimal
    if (this->humidity < 38) 
      hum_score = 0.25/40*this->humidity*100;
    else
    {
      hum_score = ((-0.25/(100-40)*this->humidity)+0.416666)*100;
    }
  }
  gas_reference = this->gas_resistance;
  float gas_lower_limit = 0;   // Bad air quality limit
  float gas_upper_limit = 100000;  // Good air quality limit 
  if (gas_reference > gas_upper_limit) gas_reference = gas_upper_limit; 
  if (gas_reference < gas_lower_limit) gas_reference = gas_lower_limit;
  gas_score = (0.75/(gas_upper_limit-gas_lower_limit)*gas_reference -(gas_lower_limit*(0.75/(gas_upper_limit-gas_lower_limit))))*100;
  float air_quality_score = hum_score + gas_score;
  return (100-air_quality_score)*5;
}

int BME680::read(){
  if (!this->initialized){
    return 1;
  }
  if (!this->bme.performReading()) {
		return 2;
	}
  this->temperature = bme.temperature;
  this->humidity = bme.humidity;
  this->pressure = bme.pressure;
  this->gas_resistance = bme.gas_resistance;
  this->iaq = this->get_iaq();
  return 0;
}
